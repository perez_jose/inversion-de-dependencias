/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inverciondedependencia;

/**
 *
 * @author CompuStore
 */
public class Jugador {
    private String nombre;
    private Jugable dispositivojugable;
    //en este momento se inicializa la invercion de dependencia ya que aqui se le da un dispositivo por parametros

    public Jugador(String nombre,Jugable dispositivojugable) {
        this.nombre = nombre;
    this.dispositivojugable=dispositivojugable;
    }
    public void jugar(){
        dispositivojugable.jugar();
    }
    
    
}
